#!/usr/bin/env python
# coding:utf-8

import time
from gae import __version__

def application(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/plain; charset=UTF-8')])
    if environ['PATH_INFO'] == '/robots.txt':
        yield '\n'.join(['User-agent: *', 'Disallow: /'])
    else:
        timestamp = long(environ['CURRENT_VERSION_ID'].split('.')[1])/2**28
        ctime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(timestamp+8*3600))
        yield "The first try depends on Google Could Platform. hope to build a website about app DIARY/Growing DIARY that applies family and growing kids [%s]." % (ctime)
